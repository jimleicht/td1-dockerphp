# Utilisez une image de base contenant Apache et PHP
FROM php:8.2-apache

# Copiez votre code PHP dans le répertoire /var/www/html/ du conteneur
COPY index.php /var/www/html/index.php

# Exposez le port 80 pour le serveur web Apache
EXPOSE 80

# Démarrez Apache au lancement du conteneur
CMD ["apache2-foreground"]